import React from "react";
import Countries from "../components/Countries";
import Layout from "../components/Layout";

const Home = () => {
  return (
    <Layout nameClass={""}>
      <Countries />
    </Layout>
  );
};

export default Home;
