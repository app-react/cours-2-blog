
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Article from '../components/Article';
import Layout from '../components/Layout';

const Blog = () => {
  
  const callAPI = axios.create({
    baseURL: 'http://localhost:3004/articles'
  })

  const [blogData, setBlogData] = useState([])
  const [author, setAuthor]     = useState('');
  const [content, setContent]   = useState('');
  const [error, setError]       = useState(false);

  useEffect( async () => await getData(), [])
  const getData = async () => {
    await callAPI.get('').then(resp => setBlogData(resp.data))
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    if(content.length < 140) {
      setError(true)
    } else {
      setError(false)

      await callAPI.post('', {
        author,
        content,
        date   : Date.now()
      })

      setAuthor('')
      setContent('')
      await getData()
    }
  }

  return (
    <Layout nameClass={"blog-container"}>
      <h1>Blog</h1>
      <form onSubmit={(e)=> handleSubmit(e)}>

        <input 
          type="text" 
          placeholder="Nom" 
          onInput={(e) => setAuthor(e.target.value)}
          value={author}
        />

        <textarea 
          placeholder="Message" 
          onChange={(e) => setContent(e.target.value)}
          style={{ border : error && "1px solid red" }}
          value={content}
        ></textarea>
        {error && <p>Veuillez écrire un minimum de 140 caractères</p> }
        
        <input type="submit" value="Envoyer" />
      </form>

      <ul>
        { 
          blogData
            .sort((a,b)=>b.date-a.date)
            .map(blog => ( <Article key={blog.id} article={blog} /> )) 
        }
      </ul>
    </Layout>
  );
};

export default Blog;