import React, { useState } from 'react';
import axios from 'axios'

const Article = ({article}) => {

  const callAPI = axios.create({
    baseURL: 'http://localhost:3004/articles'
  })

  const [isEditing, setIsEditing]     = useState(false)
  const [editContent, setEditContent] = useState('');
  const [updateAt, setUpdateAt]       = useState(null);

  const dateFormat = (date) => {
    return new Date(date).toLocaleDateString("fr-FR", {
      year  : 'numeric',
      month : 'long',
      day   : 'numeric',
      hour  : 'numeric',
      minute: 'numeric',
      second: 'numeric',
    })
  }

  const handleEdit = async () => {
    const data = {
      author     : article.author,
      content    : editContent ? editContent: article.content,
      updatedDate: Date.now()
    }
    await callAPI.patch(`/${article.id}`, data)

    setUpdateAt(Date.now())
    setIsEditing(false)
  }

  const handleDelete = () => {
    callAPI.delete(`${article.id}`)
    window.location.reload()
  }

  return (
    <article className="article" style={{background: isEditing && '#f3feff'}}>
      <header className="card-header">
        <h3>{article.author}</h3>
        <div className="card-date">
          <em>Posté le {dateFormat(article.date)}</em>
          { 'updatedDate' in article || updateAt  ? (
              updateAt ? (
                <em>Modifié le {dateFormat(updateAt)}</em>
              ) : (
                <em>Modifié le {dateFormat(article.updatedDate)}</em>
              )
            ) : ''
          }
        </div>
      </header>
      
      {
        isEditing 
          ? (
            <textarea 
              placeholder="Message" 
              onChange={(e) => setEditContent(e.target.value)}
              defaultValue={editContent ? editContent : article.content}
              autoFocus
            ></textarea>
          )
          : (
            <p>{editContent ? editContent : article.content}</p>
          )
      }


      <div className="btn-container">
      {isEditing  ? (
          <button onClick={()=>handleEdit()}>Valider</button>
        ) : (
          <button onClick={(e)=>setIsEditing(true)}>Edit</button>
        )
      }
        <button onClick={()=> handleDelete()}>Suppr.</button>
      </div>
    </article>
  );
};

export default Article;