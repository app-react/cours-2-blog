import React from 'react';
import Logo from "../components/Logo";
import Navigation from "../components/Navigation";

const Layout = ({children, nameClass}) => {
  return (
    <div className={nameClass}>
      <Logo />
      <Navigation />
      {children}
    </div>
  );
};

export default Layout;